<?php
/**
 * @file
 * pushtape_discography_advanced.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function pushtape_discography_advanced_taxonomy_default_vocabularies() {
  return array(
    'record_label' => array(
      'name' => 'Record label',
      'machine_name' => 'record_label',
      'description' => 'Associate tracks and albums with record labels.',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => -7,
      'rdf_mapping' => array(
        'rdftype' => array(
          0 => 'skos:ConceptScheme',
        ),
        'name' => array(
          'predicates' => array(
            0 => 'dc:title',
          ),
        ),
        'description' => array(
          'predicates' => array(
            0 => 'rdfs:comment',
          ),
        ),
      ),
    ),
  );
}
